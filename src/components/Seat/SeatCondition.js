import React from "react";

const dataStatus = ["Available", "VIP", "Unavailable", "Selected"];

export default function SeatCondition() {
  return (
    <div id="legend" className="seatCharts-legend flex justify-center">
      <div className="grid grid-cols-2 gap-x-20 seatCharts-legendList text-white">
        {dataStatus.map((item, index) => {
          return (
            <div className="seatCharts-legendItem" key={index}>
              <div
                className={`seatCharts-seat seatCharts-cell ${item.toLowerCase()}`}
              />
              <span className="seatCharts-legendDescription">{item}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
}
