import { Carousel } from "flowbite-react";
import "./carousel.style.css";
import React, { useEffect } from "react";
import { movieService } from "../../services/movieService";
import { useState } from "react";


export default function CarouselMovie() {
  const [movieBanner, setMovieBanner] = useState([]);
  useEffect(() => {
    movieService
      .getBannerMovie()
      .then((res) => {
        console.log(res.data.content);
        return setMovieBanner(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContentBannerMovie = () => {
    return movieBanner.map((item) => {
      return (
        <img
          key={item.maPhim}
          className=" h-full w-full object-cover  top-[12rem] md:top-60 xl:top-80"
          src={item.hinhAnh}
          alt={item.hinhAnh}
        />
      );
    });
  };

  return (
    <div className=" h-72 md:h-[40vh] bg-slate-900  w-5/5 overflow-hidden xl:h-[80vh] 2xl:h-screen container mx-auto  ">
      <Carousel id="carrousel_banner" slideInterval={5000}>{renderContentBannerMovie()}</Carousel>
    </div>
  );
}
