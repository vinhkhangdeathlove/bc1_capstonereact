import "antd/dist/antd.css";
import { LoginPage } from "./Pages/LoginPage/LoginPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./HOC/Layout/Layout";
import HomePage from "./Pages/HomePage/HomePage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import DetailMoviePage from "./Pages/DetailPage/DetailMoviePage";
import PurchasePage from "./Pages/PurchasePage/PurchasePage";
import "./App.css";

function App() {
  return (
    <div className="App min-h-screen max-h-full">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailMoviePage} />}
          />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/purchase/:param"
            element={<Layout Component={PurchasePage} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
