import thunk from "redux-thunk";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { userReducer } from "./reducers/userReducer";
import { movieReducer } from "./reducers/movieReducer";
import { seatReducer } from "./reducers/seatReducer";

const rootReducer = combineReducers({
  userReducer,
  movieReducer,
  seatReducer,
});

const composeEnhanser = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(
  rootReducer,
  composeEnhanser(applyMiddleware(thunk))
);
