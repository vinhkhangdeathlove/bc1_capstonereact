import React, { Component } from "react";
import CarouselMovie from "../../components/Carousel/CarouselMovie";
import ListMovie from "./ListMovie";
import News from "./News";
import TabMovie from "./TabMovie";

export default class HomePage extends Component {
  render() {
    return (
      <div className="">
        <div div className=" rounded-lg md:p-5 ">
          <CarouselMovie />
        </div>
        <div >
          <ListMovie />
        </div>
        <TabMovie />
        <div>
          <News />
        </div>
      </div>
    );
  }
}
